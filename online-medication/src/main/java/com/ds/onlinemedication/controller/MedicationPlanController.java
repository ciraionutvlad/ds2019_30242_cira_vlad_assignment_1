package com.ds.onlinemedication.controller;

import com.ds.onlinemedication.dto.MedicationPlanDTO;
import com.ds.onlinemedication.service.MedicationPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/")
public class MedicationPlanController {

    private MedicationPlanService medicationPlanService;

    @Autowired
    public MedicationPlanController(MedicationPlanService medicationPlanService) {
        this.medicationPlanService = medicationPlanService;
    }
    @GetMapping("add-medication-plan")
    public String showAddDoctor(MedicationPlanDTO medicationPlanDTO) {
        return "add-medication-plan";
    }
    @GetMapping(value = "medication_plan/{id}")
    public MedicationPlanDTO findById(@PathVariable("id") Integer id) {
        return medicationPlanService.findById(id);
    }

    @GetMapping(value = "medication_plan/all")
    public String findAll(Model model) {
        List<MedicationPlanDTO> list = medicationPlanService.findAll();
        model.addAttribute("medicationPlans", list);
        return "list-medicationPlan";
    }

    @PostMapping(value = "medication_plan/add")
    public Integer insert(@RequestBody MedicationPlanDTO medicationPlanDTO) {
        return medicationPlanService.insert(medicationPlanDTO);
    }

    @PutMapping(value = "medication_plan/update")
    public Integer update(@RequestBody MedicationPlanDTO medicationPlanDTO) {
        return medicationPlanService.update(medicationPlanDTO);
    }

    @DeleteMapping(value = "medication_plan/delete")
    public void delete(@RequestBody MedicationPlanDTO medicationPlanDTO) {
        medicationPlanService.delete(medicationPlanDTO);
    }

}

package com.ds.onlinemedication.controller;

import com.ds.onlinemedication.dto.MedicationDTO;
import com.ds.onlinemedication.service.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(value = "/")
public class MedicationController {

    private MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping("add-medication")
    public String showAddMedication(MedicationDTO medicationDTO) {
        return "add-medication";
    }

    @GetMapping("medication/edit/{id}")
    public String showUpdateForm(@PathVariable("id") int id, Model model) {
        MedicationDTO medicationDTO = medicationService.findById(id);
        model.addAttribute("medication", medicationDTO);
        return "update-medication";
    }

    @GetMapping(value = "medication/{id}")
    public MedicationDTO findById(@PathVariable("id") Integer id) {
        return medicationService.findById(id);
    }

    @GetMapping(value = "medication/all")
    public String findAll(Model model) {
        List<MedicationDTO> list = medicationService.findAll();
        model.addAttribute("medications", list);
        return "list-medication";
    }

    @PostMapping(value = "medication/add")
    public String insert(@Valid MedicationDTO medicationDTO, Model model) {
        medicationService.insert(medicationDTO);
        model.addAttribute("medications", medicationService.findAll());
        return "list-medication";
    }

    @PostMapping(value = "medication/update/{id}")
    public String update(@PathVariable("id") int id, @Valid MedicationDTO medicationDTO, Model model) {
        medicationDTO.setId(id);
        medicationService.update(medicationDTO);
        model.addAttribute("medications", medicationService.findAll());
        return "list-medication";
    }

    @GetMapping(value = "medication/delete/{id}")
    public String delete(@PathVariable("id") Integer id, Model model) {
        medicationService.delete(id);
        model.addAttribute("medications", medicationService.findAll());
        return "list-medication";
    }
}

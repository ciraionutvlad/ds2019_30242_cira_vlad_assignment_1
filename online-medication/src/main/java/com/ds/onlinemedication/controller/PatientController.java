package com.ds.onlinemedication.controller;

import com.ds.onlinemedication.dto.CaregiverDTO;
import com.ds.onlinemedication.dto.PatientDTO;
import com.ds.onlinemedication.service.CaregiverService;
import com.ds.onlinemedication.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(value = "/")
public class PatientController {

    private PatientService patientService;
    @Autowired
    private CaregiverService caregiverService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping("add-patient")
    public String showAddDoctor(PatientDTO patientDTO) {
        return "add-patient";
    }

    @GetMapping("patient/edit/{id}")
    public String showUpdateForm(@PathVariable("id") int id, Model model) {
        PatientDTO patientDTO = patientService.findById(id);
        model.addAttribute("patient", patientDTO);
        return "update-patient";
    }

    @GetMapping(value = "patient/{id}")
    public PatientDTO findById(@PathVariable("id") Integer id) {
        return patientService.findById(id);
    }

    @GetMapping(value = "patient/all")
    public String findAll(Model model) {
        List<PatientDTO> list = patientService.findAll();
        model.addAttribute("patients", list);
        return "list-patient";
    }

    @PostMapping(value = "patient/add")
    public String insert(@Valid PatientDTO patientDTO, Model model) {
        CaregiverDTO caregiverDTO = caregiverService.findById(patientDTO.getCaregiverId());
        patientDTO.setCaregiver(caregiverDTO);
        patientService.insert(patientDTO);
        model.addAttribute("patients", patientService.findAll());
        return "list-patient";
    }

    @PostMapping(value = "patient/update/{id}")
    public String update(@PathVariable("id") int id, @Valid PatientDTO patientDTO, Model model) {
        CaregiverDTO caregiverDTO = caregiverService.findById(patientDTO.getCaregiverId());
        patientDTO.setCaregiver(caregiverDTO);
        patientDTO.setId(id);
        patientService.update(patientDTO);
        model.addAttribute("patients", patientService.findAll());
        return "list-patient";
    }

    @GetMapping(value = "patient/delete/{id}")
    public String delete(@PathVariable("id") Integer id, Model model) {
        patientService.delete(id);
        model.addAttribute("patients", patientService.findAll());
        return "list-patient";
    }
}

package com.ds.onlinemedication.repositories;

import com.ds.onlinemedication.entities.Caregiver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface CaregiverRepository extends JpaRepository<Caregiver, Integer> {

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("UPDATE Caregiver d SET d.name = :name, d.birthDate = :birthDate, d.gender = :gender, " +
            "d.address = :address, d.listOfPatientsTakenCareOf = :listOfPatientsTakenCareOf WHERE d.id = :id")
    int updateCaregiver(@Param("id") int id,
                        @Param("name") String name,
                        @Param("birthDate") String birthDate,
                        @Param("gender") String gender,
                        @Param("address") String address,
                        @Param("listOfPatientsTakenCareOf") String listOfPatientsTakenCareOf);
}

package com.ds.onlinemedication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineMedicationApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineMedicationApplication.class, args);
    }

}

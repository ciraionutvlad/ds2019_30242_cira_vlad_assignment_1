package com.ds.onlinemedication.dto;

public class CaregiverDTO {

    private Integer id;
    private String name;
    private String birthDate;
    private String gender;
    private String address;
    private String listOfPatientsTakenCareOf;

    public CaregiverDTO(Integer id, String name, String birthDate, String gender, String address, String listOfPatientsTakenCareOf) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.listOfPatientsTakenCareOf = listOfPatientsTakenCareOf;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public String getGender() {
        return gender;
    }

    public String getAddress() {
        return address;
    }

    public String getListOfPatientsTakenCareOf() {
        return listOfPatientsTakenCareOf;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CaregiverDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", gender='" + gender + '\'' +
                ", address='" + address + '\'' +
                ", listOfPatientsTakenCareOf='" + listOfPatientsTakenCareOf + '\'' +
                '}';
    }
}

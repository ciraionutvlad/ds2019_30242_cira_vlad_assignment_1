package com.ds.onlinemedication.service;

import com.ds.onlinemedication.dto.PatientBuilder;
import com.ds.onlinemedication.dto.PatientDTO;
import com.ds.onlinemedication.entities.Patient;
import com.ds.onlinemedication.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public PatientDTO findById(Integer id) {
        Optional<Patient> patient = patientRepository.findById(id);

        if (!patient.isPresent()) {
            throw new ResourceNotFoundException("Patient: " + id);
        }

        return PatientBuilder.generatePatientDTO(patient.get());
    }

    public List<PatientDTO> findAll() {
        return patientRepository.findAll()
                .stream()
                .map(PatientBuilder::generatePatientDTO)
                .collect(Collectors.toList());
    }

    public Integer insert(PatientDTO patientDTO) {
        return patientRepository
                .save(PatientBuilder.generatePatient(patientDTO))
                .getId();
    }

    public Integer update(PatientDTO patientDTO) {
        Optional<Patient> patient = patientRepository.findById(patientDTO.getId());

        if (!patient.isPresent()) {
            throw new ResourceNotFoundException("Patient" + patientDTO.getId().toString());
        }

        Patient newPatient = PatientBuilder.generatePatient(patientDTO);
        return patientRepository.updatePatient(newPatient.getId(), newPatient.getName(),
                newPatient.getBirthDate(), newPatient.getGender(), newPatient.getAddress(),
                newPatient.getMedicalRecord(), newPatient.getCaregiver());
    }

    public void delete(int patientId) {
        patientRepository.deleteById(patientId);
    }
}

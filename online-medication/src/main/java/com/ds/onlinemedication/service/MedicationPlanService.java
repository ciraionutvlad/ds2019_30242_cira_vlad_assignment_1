package com.ds.onlinemedication.service;

import com.ds.onlinemedication.dto.MedicationPlanBuilder;
import com.ds.onlinemedication.dto.MedicationPlanDTO;
import com.ds.onlinemedication.entities.MedicationPlan;
import com.ds.onlinemedication.repositories.MedicationPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {

    private MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
    }

    public MedicationPlanDTO findById(Integer id) {
        Optional<MedicationPlan> medicationPlan = medicationPlanRepository.findById(id);

        if (!medicationPlan.isPresent()) {
            throw new ResourceNotFoundException("MedicationPlan: " + id);
        }

        return MedicationPlanBuilder.generateMedicationPlanDTO(medicationPlan.get());
    }

    public List<MedicationPlanDTO> findAll() {
        return medicationPlanRepository.findAll()
                .stream()
                .map(MedicationPlanBuilder::generateMedicationPlanDTO)
                .collect(Collectors.toList());
    }

    public Integer insert(MedicationPlanDTO medicationPlanDTO) {
        return medicationPlanRepository
                .save(MedicationPlanBuilder.generateMedicationPlan(medicationPlanDTO))
                .getId();
    }

    public Integer update(MedicationPlanDTO medicationPlanDTO) {
        Optional<MedicationPlan> medicationPlan = medicationPlanRepository.findById(medicationPlanDTO.getId());

        if (!medicationPlan.isPresent()) {
            throw new ResourceNotFoundException("MedicationPlan" + medicationPlanDTO.getId().toString());
        }

        MedicationPlan newMedicationPlan = MedicationPlanBuilder.generateMedicationPlan(medicationPlanDTO);
        return medicationPlanRepository.updateMedicationPlan(newMedicationPlan.getId(), newMedicationPlan.getDiagnostic(),
                newMedicationPlan.getIntakeIntervals(), newMedicationPlan.getPeriodOfTheTreatment(),
                newMedicationPlan.getListOfMedication());
    }

    public void delete(MedicationPlanDTO medicationPlanDTO) {
        medicationPlanRepository.deleteById(medicationPlanDTO.getId());
    }
}
